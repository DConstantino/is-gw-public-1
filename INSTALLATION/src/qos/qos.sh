#!/bin/bash

set -a
. <LOCAL_ETC_DIR>/dynamic_vars --source-only
set +a


if [ "$#" -ne 1 ]; then
	echo -e "Usage: cake_script {start|stop|restart|status}\n"
	exit
fi

case "$1" in
	start)
		## TODO iniciar traffic shapping
		tc qdisc add dev $WAN_IFACE root cake bandwidth ${WAN_UPSTREAM}mbit
		echo `date` > /tmp/qos_active
	;;
	stop)
		## TODO limpar traffic shapping
		tc qdisc del dev $WAN_IFACE root
		rm /tmp/qos_active > /dev/null 2>&1 
	;;

	restart)
		## TODO limpar e iniciar
		tc qdisc del dev $WAN_IFACE root
		rm /tmp/qos_active > /dev/null 2>&1
		tc qdisc add dev $WAN_IFACE root cake bandwidth ${WAN_UPSTREAM}mbit
		echo `date` > /tmp/qos_active
		;;

	status)
		## TODO dar o estado das interfaces
		if [ -f /tmp/qos_active ]; then
			echo ""
			echo "Cake params:"
			echo ""
			tc qdisc show dev $WAN_IFACE | awk '{print "\t" $7 ": " $8}'
			tc -s qdisc show dev $WAN_IFACE | awk '$1 == "interval" || $1 == "Av-delay" || $1 == "drops" || $1 == "target" {print "\t" $1 ": " $3;}'
			echo ""
		else
			echo ""
			echo "QoS is inactive!"
			echo ""
		fi   
		;;

	*)
		echo -e "Usage: cake_script {start|stop|restart|status}\n"
	exit 1
esac

exit
