#!/bin/bash
#configuration file
##### ENVIRONMENT SETUP #####


INPUT_FILE="./gw.conf"
#INPUT_FILE=$1

WAIT_FOR_PING=5
WAIT_FOR_IFDOWN=5

if [ ! -f $INPUT_FILE ]; then
    echo "File '$1' does not exist!"
    exit
fi

set -a
. $INPUT_FILE
set +a

echo "Setting up installation..."
while true; do
	echo "#######################"
	echo "INPUT FILE: $INPUT_FILE"
	echo "#######################"
    read -p "Do you wish to continue with the installation? (yes no) " yn
    case $yn in
        "Yes" | "yes" | "YES" ) echo "######### STARTING INSTALLATION ##########"; break;;
        "No" | "no" | "NO") exit;;
        * ) echo "Please answer yes or no.";;
    esac
done


echo "Checking for friendly environment..."
sleep $WAIT_FOR_PING

if [[ $SSH_GROUPS_LOCAL = "" ]]; then
	echo "Variable SSH_GROUPS_LOCAL in file $INPUT_FILE cannot be empty!"
	exit;
fi

ping 8.8.8.8 -c 5
if [[ $? -eq 1 ]]; then
	echo "Could not reach Internet!"
	echo "EXITING: non friendly environment!"
	exit;
fi

if [[ $DOMAIN_SERVER_IP != "" && $WINBIND -eq 1 ]]; then
	echo "Pinging DOMAIN SERVER (${DOMAIN_SERVER_IP})...."
	ping $DOMAIN_SERVER_IP -c 5
	if [[ $? -eq 1 ]]; then
		echo "Could not ping ${DOMAIN_SERVER} at ${DOMAIN_SERVER_IP}, please make sure the server its avaliable for proper installation!"
		echo "EXITING: non friendly environment!"
		exit;
	fi
fi

WAN_UTILS="./bin/wan_utils.sh"
INTERFACE_SETUP="./bin/interface_setup.sh"
INTERFACE_AUX="./bin/interface_aux.sh"
DHCP_SETUP="./bin/dhcp_setup.sh"

. ${INTERFACE_SETUP} --source-only
. ${DHCP_SETUP} --source-only
. ${WAN_UTILS} --source-only

#############################
# replace function
replace_expr() {

	FILE=$1
	OLDEXP=$2
	NEWEXP=$3
	echo replacing $OLDEXP by $NEWEXP on $FILE


	#sed -i "s/$OLDEXP/$NEWEXP/g" $FILE
	sed -i "s@$OLDEXP@$NEWEXP@g" $FILE
}

replace_for_multiple(){
	
	FILE=$1
	OLDEXP=$2
	NEWEXP=$(echo "${@:3}")
	echo replacing $OLDEXP by $NEWEXP on $FILE


	sed -i "s/$OLDEXP/$NEWEXP/g" $FILE
}

##### MARKING INSTALLATON VERSION #####
echo "STRTESTING" > /etc/as.version
#######################################

##### FIXING STARTUP DELAY #####
replace_expr /etc/fstab "UUID=73a72fa1-0790-4745-a5a4-1d8743872528" "#UUID=73a72fa1-0790-4745-a5a4-1d8743872528"
################################

##### CREATE LOCAL AS DIRECTORY #####
# AS
if [[ ! -d "$LOCAL_DIR" ]]; then
	mkdir $LOCAL_DIR
fi
# AS/bin
if [[ ! -d "$LOCAL_BIN_DIR" ]]; then
	mkdir $LOCAL_BIN_DIR
fi
# AS/etc
if [[ ! -d "$LOCAL_ETC_DIR" ]]; then
	mkdir $LOCAL_ETC_DIR
fi
# AS/lib or custom libdir
if [[ ! -d "$LOCAL_LIB_DIR" ]]; then
    mkdir $LOCAL_LIB_DIR
fi

#####################################
echo "SETTING HOSTNAME.....!!!!!"
hostnamectl set-hostname $HOST_NAME
grep $HOST_NAME /etc/hosts || echo "127.0.0.1 localhost localhost.localdomain ${HOST_NAME}" >> /etc/hosts

#####################################
DYNAMIC_VARS_FILE="./src/etc/dynamic_vars"
cp ${DYNAMIC_VARS_FILE} ${DYNAMIC_VARS_FILE}_temp

echo "########## DONT CHANGE ##########" >> ${DYNAMIC_VARS_FILE}_temp
echo "WAN_IFACE=${WAN_IFACE} # dont change" >> ${DYNAMIC_VARS_FILE}_temp
echo "WAN_METHOD=${WAN_METHOD} # dont change" >> ${DYNAMIC_VARS_FILE}_temp
echo "WAN_NETWORK=${WAN_NETWORK} # dont change" >> ${DYNAMIC_VARS_FILE}_temp
echo "#----------------------------------------------" >> ${DYNAMIC_VARS_FILE}_temp
echo "WAN_BKP_IFACE=${WAN_BKP_IFACE} # dont change" >> ${DYNAMIC_VARS_FILE}_temp
echo "WAN_BKP_METHOD=${WAN_BKP_METHOD} # dont change" >> ${DYNAMIC_VARS_FILE}_temp
echo "WAN_BKP_NETWORK=${WAN_BKP_NETWORK} # dont change" >> ${DYNAMIC_VARS_FILE}_temp
echo "#----------------------------------------------" >> ${DYNAMIC_VARS_FILE}_temp
echo "LAN_IFACE=${LAN_IFACE} # dont change" >> ${DYNAMIC_VARS_FILE}_temp
echo "LAN_NETWORK=${LAN_NETWORK} # dont change" >> ${DYNAMIC_VARS_FILE}_temp
echo "#----------------------------------------------" >> ${DYNAMIC_VARS_FILE}_temp
echo "LAN_AUX_IFACE=${LAN_AUX_IFACE} # dont change" >> ${DYNAMIC_VARS_FILE}_temp
echo "LAN_AUX_NETWORK=${LAN_AUX_NETWORK} # dont change" >> ${DYNAMIC_VARS_FILE}_temp
echo "#----------------------------------------------" >> ${DYNAMIC_VARS_FILE}_temp
echo "WIRELESS_IFACE=${WIRELESS_IFACE} # dont change" >> ${DYNAMIC_VARS_FILE}_temp
echo "WIRELESS_NETWORK=${WIRELESS_NETWORK} # dont change" >> ${DYNAMIC_VARS_FILE}_temp
echo "#----------------------------------------------" >> ${DYNAMIC_VARS_FILE}_temp
echo "#################################" >> ${DYNAMIC_VARS_FILE}_temp
echo 'WAN_UPSTREAM="${WAN_UPSTREAM}"' >> ${DYNAMIC_VARS_FILE}_temp
echo 'WAN_BKP_UPSTREAM="${WAN_BKP_UPSTREAM}"' >> ${DYNAMIC_VARS_FILE}_temp
echo 'SERVERS="${SERVERS}"' >> ${DYNAMIC_VARS_FILE}_temp
echo 'TARGET_SERVERS="${TARGET_SERVERS}"' >> ${DYNAMIC_VARS_FILE}_temp

envsubst < ${DYNAMIC_VARS_FILE}_temp > /tmp/temp_dyvars
cp /tmp/temp_dyvars ${DYNAMIC_VARS_FILE}_temp


cp ${DYNAMIC_VARS_FILE}_temp $LOCAL_ETC_DIR/dynamic_vars
cp ${WAN_UTILS} $LOCAL_BIN_DIR

rm ${DYNAMIC_VARS_FILE}_temp
rm /tmp/temp_dyvars

chmod 644 "$LOCAL_ETC_DIR/dynamic_vars"
chmod 755 "$LOCAL_BIN_DIR/wan_utils.sh"
#####################################
# wan configuration files
mkdir ${LOCAL_LIB_DIR}/wan_config

cp $WAN_CONFIG_DIR/start_setup.sh ${LOCAL_LIB_DIR}/wan_config
cp $WAN_CONFIG_DIR/main_logic.sh ${LOCAL_LIB_DIR}/wan_config
cp $WAN_CONFIG_DIR/source_file ${LOCAL_LIB_DIR}/wan_config
cp $INTERFACE_AUX ${LOCAL_LIB_DIR}

replace_expr ${LOCAL_LIB_DIR}/wan_config/start_setup.sh "<LOCAL_LIB_DIR>" $LOCAL_LIB_DIR
replace_expr ${LOCAL_LIB_DIR}/wan_config/start_setup.sh "<LOCAL_ETC_DIR>" $LOCAL_ETC_DIR

replace_expr ${LOCAL_LIB_DIR}/wan_config/main_logic.sh "<LOCAL_LIB_DIR>" $LOCAL_LIB_DIR
replace_expr ${LOCAL_LIB_DIR}/wan_config/main_logic.sh "<LOCAL_ETC_DIR>" $LOCAL_ETC_DIR
replace_expr ${LOCAL_LIB_DIR}/wan_config/main_logic.sh "<LOCAL_BIN_DIR>" $LOCAL_BIN_DIR

chmod 755 ${LOCAL_LIB_DIR}/wan_config/start_setup.sh
chmod 755 ${LOCAL_LIB_DIR}/wan_config/main_logic.sh
chmod 644 ${LOCAL_LIB_DIR}/wan_config/source_file
chmod 755 ${LOCAL_LIB_DIR}/interface_aux.sh

ln -s ${LOCAL_LIB_DIR}/wan_config/start_setup.sh ${LOCAL_BIN_DIR}/config_wan_iface.sh
#####################################
# configuration log file
LOG_TEXT="# LOG FILE: $(date)"
echo $LOG_TEXT > $LOG_FILE

##### enable forwarding ipv4 #####
##### /etc/sysctl.d/99-sysctl.conf #####
replace_expr /etc/sysctl.d/99-sysctl.conf "#net.ipv4.ip_forward=1" "net.ipv4.ip_forward=1"
sysctl -p /etc/sysctl.d/99-sysctl.conf

##### /etc/modules #####
cp $ETC_MODULES_FILE /etc/modules

##### /etc/apt #####
cp $APT_SOURCES_FILE /etc/apt

chmod 644 /etc/apt/sources.list


export DEBIAN_FRONTEND=noninteractive

### INTERFACES SETUP ###
#----------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------------------


echo "8021q" >> /etc/modules
modprobe 8021q

############# dealing with gateways ####################
if [[ "$WAN_METHOD" = "static" ]]; then
	echo $WAN_GATEWAY > ${LOCAL_ETC_DIR}/${WAN_IFACE}-gw
fi

if [[ "$WAN_BKP_METHOD" = "static" ]]; then
	echo $WAN_BKP_GATEWAY > ${LOCAL_ETC_DIR}/${WAN_BKP_IFACE}-gw
fi
########################################################
######### rc.local #########
cp $RC_LOCAL_FILE /etc/rc.local

echo "${LOCAL_BIN_DIR}/wan_monitor.sh &" >> /etc/rc.local
echo "${LOCAL_BIN_DIR}/run_port_forwarding" >> /etc/rc.local
echo "${LOCAL_BIN_DIR}/run_lan_forwarding" >> /etc/rc.local
echo "${LOCAL_BIN_DIR}/post_apply" >> /etc/rc.local
echo "systemctl start openvpn" >> /etc/rc.local
echo "#${LOCAL_BIN_DIR}/qos.sh start" >> /etc/rc.local
echo "exit 0" >> /etc/rc.local

########################

# setup dhcp-hooks
cp $DHCP_HOOKS /etc/dhcp/dhclient-exit-hooks.d/
chmod 644 /etc/dhcp/dhclient-exit-hooks.d/dhcp-hooks
########################


##### the interfaces file #####
iface_setup > ${INTERFACES_FILE}_temp

##### CLEAN UP #####
cp ${INTERFACES_FILE}_temp ${INTERFACES_FILE}
rm ${INTERFACES_FILE}_temp

chmod 644 ${INTERFACES_FILE}

echo "RESTARTING NETWORKING SERVICE..."
ip addr flush $LAN_BRIDGE_PORT &> /dev/null
systemctl restart networking.service &> /dev/null


#----------------------------------------------------------------------------------------------------------------
# routes on up setup file placement
rm /tmp/clean_routes &> /dev/null
cp $ROUTES_ON_UP_FILE /etc/network/if-up.d/
replace_expr "/etc/network/if-up.d/routes_on_up" "<LOCAL_ETC_DIR>" $LOCAL_ETC_DIR
replace_expr "/etc/network/if-up.d/routes_on_up" "<LOCAL_BIN_DIR>" $LOCAL_BIN_DIR

chmod 755 "/etc/network/if-up.d/routes_on_up"

#----------GET GATEWAYS------------
WAN_GW=`cat /${LOCAL_ETC_DIR}/${WAN_IFACE}-gw &> /dev/null`
WAN_BKP_GW=`cat /${LOCAL_ETC_DIR}/${WAN_BKP_IFACE}-gw &> /dev/null`
#----------------------------------

#-----CHECK DOUBLE WAN AND SET RT_TABLES----------
cp $RT_TABLES_SKEL ${RT_TABLES_FILE}_temp

ip rule flush &> /dev/null
ip rule add from 0.0.0.0/0 lookup main pref 32766 &> /dev/null
ip rule add from 0.0.0.0/0 lookup default pref 32767 &> /dev/null
echo `date` > /tmp/clean_routes

check_double_wan
double_wan_result=$?

if [[ double_wan_result -eq 0 ]]; then
	echo "1	${WAN_ROUTING_TABLE}" >> ${RT_TABLES_FILE}_temp
	echo "2	${WAN_BKP_ROUTING_TABLE}" >> ${RT_TABLES_FILE}_temp
	set_wan_routes >& /dev/null
	set_wan_bkp_routes >& /dev/null

elif [[ double_wan_result -eq 1 ]]; then
	echo "1	${WAN_ROUTING_TABLE}" >> ${RT_TABLES_FILE}_temp
	set_wan_routes >& /dev/null
	
elif [[ double_wan_result -eq 2 ]]; then
	echo  "BAD CONFIG FILE: WAN or WAN_BKP method." >> $LOG_FILE
	echo "BAD CONFIG FILE!!!"
fi

# set routes for special severs
set_servers_routes &> /dev/null

cp ${RT_TABLES_FILE}_temp ${RT_TABLES_FILE}
rm ${RT_TABLES_FILE}_temp

chmod 644 ${RT_TABLES_FILE}

#------------------------------------------------

#-------------SETUP WAN MONITOR FILES------------
cp ${WAN_MONITOR_AUX_FILE}_skel ${WAN_MONITOR_AUX_FILE}_temp

replace_expr ${WAN_MONITOR_AUX_FILE}_temp "<TARGET_SERVERS>" "${TARGET_SERVERS}"
replace_expr ${WAN_MONITOR_AUX_FILE}_temp "<WAN_IFACE>" "${WAN_IFACE}"
replace_expr ${WAN_MONITOR_AUX_FILE}_temp "<WAN_BKP_IFACE>" "${WAN_BKP_IFACE}"
replace_expr ${WAN_MONITOR_AUX_FILE}_temp "<WAN_SWITCH>" "${WAN_SWITCH}"
replace_expr ${WAN_MONITOR_AUX_FILE}_temp "<WAN_BKP_SWITCH>" "${WAN_BKP_SWITCH}"

cp ${WAN_MONITOR_FILE}_skel ${WAN_MONITOR_FILE}_temp

replace_expr ${WAN_MONITOR_FILE}_temp "<LOCAL_BIN_DIR>" "${LOCAL_BIN_DIR}"
replace_expr ${WAN_MONITOR_FILE}_temp "<LOCAL_ETC_DIR>" "${LOCAL_ETC_DIR}"

cp ${WAN_MONITOR_AUX_FILE}_temp ${LOCAL_BIN_DIR}/wan_monitor_aux.sh
rm ${WAN_MONITOR_AUX_FILE}_temp

cp ${WAN_MONITOR_FILE}_temp ${LOCAL_BIN_DIR}/wan_monitor.sh
rm ${WAN_MONITOR_FILE}_temp

chmod 755 "${LOCAL_BIN_DIR}/wan_monitor_aux.sh"
chmod 755 "${LOCAL_BIN_DIR}/wan_monitor.sh"
#------------------------------------------------


#----------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------------------




echo "UPDATING APT-GET..."
apt-get update
echo "FINISHED UPDATING APT-GET."


##### scripts related to updates #####

# need to install aptitude
echo "INSTALLING APTITUDE..."

apt-get install -y aptitude

cp $SEC_SOURCES_LIST /etc/apt
chmod 644 /etc/apt/security.sources.list

cp $DEB_FIND_UPDATES $LOCAL_BIN_DIR
chmod 755 $LOCAL_BIN_DIR/deb-find-updates.sh

cp $APT_UPDATE_PKG $LOCAL_BIN_DIR
cp $APT_UPDATE_SURFACE $LOCAL_BIN_DIR

chmod 755 $LOCAL_BIN_DIR/apt-update-pkg.sh
chmod 755 $LOCAL_BIN_DIR/apt-update-surface.sh

######################################


echo "UPDATING OPENSSH-SERVER..."
apt-get install -y openssh-server


### DHCP SETUP ###
#----------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------------------

echo "INSTALLING ISC DHCP SERVER..."
apt-get install -y isc-dhcp-server

cp $DHCP_CONF_SKEL_FILE ${DHCP_CONF_FILE}_temp

if [[ $LAN_DHCP -eq 1 && valid_dhcp_conf -eq 0 && $WINBIND -eq 0 ]]; then

	cat $DHCP_LAN_POOL >> ${DHCP_CONF_FILE}_temp

	replace_expr ${DHCP_CONF_FILE}_temp "<LAN_NETWORK>" $LAN_NETWORK
	replace_expr ${DHCP_CONF_FILE}_temp "<LAN_NETMASK>" $LAN_NETMASK
	replace_expr ${DHCP_CONF_FILE}_temp "<LAN_STATIC_IP>" $LAN_STATIC_IP
	replace_expr ${DHCP_CONF_FILE}_temp "<DHCP_POOL_START>" $DHCP_POOL_START
	replace_expr ${DHCP_CONF_FILE}_temp "<DHCP_POOL_END>" $DHCP_POOL_END
	replace_expr ${DHCP_CONF_FILE}_temp "<DHCP_LEASE_TIME>" $DHCP_LEASE_TIME
	replace_expr ${DHCP_CONF_FILE}_temp "<DHCP_MAX_LEASE_TIME>" $DHCP_MAX_LEASE_TIME
fi

if [[ $WLAN_DHCP -eq 1 && valid_wlan_dhcp_conf -eq 0 ]]; then

	cat $DHCP_WIRELESS_POOL >> ${DHCP_CONF_FILE}_temp

	replace_expr ${DHCP_CONF_FILE}_temp "<WIRELESS_NETWORK>" $WIRELESS_NETWORK
	replace_expr ${DHCP_CONF_FILE}_temp "<WIRELESS_STATIC_IP>" $WIRELESS_STATIC_IP
	replace_expr ${DHCP_CONF_FILE}_temp "<WIRELESS_NETMASK>" $WIRELESS_NETMASK
	replace_expr ${DHCP_CONF_FILE}_temp "<WLAN_DHCP_POOL_START>" $WLAN_DHCP_POOL_START
	replace_expr ${DHCP_CONF_FILE}_temp "<WLAN_DHCP_POOL_END>" $WLAN_DHCP_POOL_END
	replace_expr ${DHCP_CONF_FILE}_temp "<WLAN_DHCP_LEASE_TIME>" $WLAN_DHCP_LEASE_TIME
	replace_expr ${DHCP_CONF_FILE}_temp "<WLAN_DHCP_MAX_LEASE_TIME>" $WLAN_DHCP_MAX_LEASE_TIME
fi

if [[ $OTHER_LAN_DHCP -eq 1 && valid_other_lan_dhcp_conf -eq 0 ]]; then

	cat $DHCP_OTHER_LAN_POOL >> ${DHCP_CONF_FILE}_temp
	
	replace_expr ${DHCP_CONF_FILE}_temp "<OTHER_LAN_DHCP_NETWORK>" $OTHER_LAN_DHCP_NETWORK
	replace_expr ${DHCP_CONF_FILE}_temp "<OTHER_LAN_DHCP_SUBNETMASK>" $OTHER_LAN_DHCP_SUBNETMASK
	replace_expr ${DHCP_CONF_FILE}_temp "<OTHER_LAN_DHCP_STATIC_IP>" $OTHER_LAN_DHCP_STATIC_IP
	replace_expr ${DHCP_CONF_FILE}_temp "<OTHER_LAN_DHCP_POOL_START>" $OTHER_LAN_DHCP_POOL_START
	replace_expr ${DHCP_CONF_FILE}_temp "<OTHER_LAN_DHCP_POOL_END>" $OTHER_LAN_DHCP_POOL_END
	replace_expr ${DHCP_CONF_FILE}_temp "<OTHER_LAN_DHCP_LEASE_TIME>" $OTHER_LAN_DHCP_LEASE_TIME
	replace_expr ${DHCP_CONF_FILE}_temp "<OTHER_LAN_DHCP_MAX_LEASE_TIME>" $OTHER_LAN_DHCP_MAX_LEASE_TIME
fi

if [[ $LAN3_DHCP -eq 1 && valid_lan3_dhcp_conf -eq 0 ]]; then
	cat $DHCP_LAN3_POOL >> ${DHCP_CONF_FILE}_temp
	
	replace_expr ${DHCP_CONF_FILE}_temp "<LAN3_DHCP_NETWORK>" $LAN3_DHCP_NETWORK
	replace_expr ${DHCP_CONF_FILE}_temp "<LAN3_DHCP_SUBNETMASK>" $LAN3_DHCP_SUBNETMASK
	replace_expr ${DHCP_CONF_FILE}_temp "<LAN3_DHCP_STATIC_IP>" $LAN3_DHCP_STATIC_IP
	replace_expr ${DHCP_CONF_FILE}_temp "<LAN3_DHCP_POOL_START>" $LAN3_DHCP_POOL_START
	replace_expr ${DHCP_CONF_FILE}_temp "<LAN3_DHCP_POOL_END>" $LAN3_DHCP_POOL_END
	replace_expr ${DHCP_CONF_FILE}_temp "<LAN3_DHCP_LEASE_TIME>" $LAN3_DHCP_LEASE_TIME
	replace_expr ${DHCP_CONF_FILE}_temp "<LAN3_DHCP_MAX_LEASE_TIME>" $LAN3_DHCP_MAX_LEASE_TIME
fi

dhcp_listen_iface_setup > $DEFAULT_ISC_DHCP_SERVER

# dhclient file
cp $DHCLIENT_FILE /etc/dhcp/

if [[ $WINBIND -eq 1 ]]; then
	replace_expr /etc/dhcp/dhclient.conf "<PUSH_DNS_IP>" $DOMAIN_SERVER_IP
else
	replace_expr /etc/dhcp/dhclient.conf "<PUSH_DNS_IP>" "127.0.0.1"
fi

##### CLEAN UP #####
cp ${DHCP_CONF_FILE}_temp ${DHCP_CONF_FILE}
rm ${DHCP_CONF_FILE}_temp

chmod 644 ${DHCP_CONF_FILE}
chmod 644 $DEFAULT_ISC_DHCP_SERVER

echo "RESTARTING DHCP SERVER..."
systemctl restart isc-dhcp-server

#----------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------------------



### DNS SETUP ###
#----------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------------------

echo "INTALLING BIND..."
apt-get install -y bind9

# /etc/bind/named.conf.options
cp $DNS_NAMED_CONF_OPTIONS_SKEL ${DNS_NAMED_CONF_OPTIONS}_temp

if [[ $LAN_NETWORK = "" ]]; then
	replace_expr ${DNS_NAMED_CONF_OPTIONS}_temp "<LAN_NETWORK>" $LAN_NETWORK
else
	replace_expr ${DNS_NAMED_CONF_OPTIONS}_temp "<LAN_NETWORK>" "${LAN_NETWORK}/24; "
fi

if [[ $LAN_AUX_NETWORK = "" ]]; then
	replace_expr ${DNS_NAMED_CONF_OPTIONS}_temp "<LAN_AUX_NETWORK>" $LAN_AUX_NETWORK
else
	replace_expr ${DNS_NAMED_CONF_OPTIONS}_temp "<LAN_AUX_NETWORK>" "${LAN_AUX_NETWORK}/24; "
fi

if [[ $LAN3_TYPE -eq 1 ]]; then
	replace_expr ${DNS_NAMED_CONF_OPTIONS}_temp "<LAN3_NETWORK>" "${LAN3_NETWORK}/24; "
else
	replace_expr ${DNS_NAMED_CONF_OPTIONS}_temp "<LAN3_NETWORK>" ""
fi

replace_expr ${DNS_NAMED_CONF_OPTIONS}_temp "<DNS_FORWARDERS>" $DNS_FORWARDERS

##### CLEAN UP #####
cp ${DNS_NAMED_CONF_OPTIONS}_temp ${DNS_NAMED_CONF_OPTIONS}
rm ${DNS_NAMED_CONF_OPTIONS}_temp


chmod 644 ${DNS_NAMED_CONF_OPTIONS}

echo "RESTARTING BIND..."
systemctl restart bind9.service
#----------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------------------

### NMAP INSTALLATION ###
#----------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------------------
echo "INTALLING NMAP..."
apt-get install -y nmap


### FIREWALL SETUP ###
#----------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------------------
echo "INTALLING IPTABLES-PERSISTENT..."

echo iptables-persistent iptables-persistent/autosave_v4 boolean false | sudo debconf-set-selections
echo iptables-persistent iptables-persistent/autosave_v6 boolean false | sudo debconf-set-selections

apt-get install -y iptables-persistent

# flushing old rules
iptables -F
iptables -t nat -F

# policy
iptables -P INPUT DROP
iptables -P FORWARD DROP
iptables -P OUTPUT ACCEPT

#Serviços
# loop back
iptables -t filter -A INPUT -i lo -j ACCEPT
# ping
iptables -t filter -A INPUT -p icmp --icmp-type echo-request  -j ACCEPT
# nc for external monitoring
iptables -A INPUT -i $WAN_IFACE -p tcp -m tcp --dport 4321 -j ACCEPT
# dhcp
iptables -A INPUT -i $LAN_IFACE -p udp --dport 67 -j ACCEPT
iptables -A INPUT -i $WIRELESS_IFACE -p udp --dport 67 -j ACCEPT
# dns
# TODO: vpn server hostname na wlan
iptables -A INPUT -i $LAN_IFACE -p udp -m udp --dport 53 -j ACCEPT
iptables -A INPUT -i $WIRELESS_IFACE -p udp -m udp --dport 53 -j ACCEPT

if [[ $LAN_AUX_METHOD = "static" ]]; then
	iptables -A INPUT -i $LAN_AUX_IFACE -p udp -m udp --dport 53 -j ACCEPT
fi


# nat
iptables -t nat -A POSTROUTING -o $WAN_IFACE -j MASQUERADE

if [[ $WAN_BKP_METHOD = "static" || $WAN_BKP_METHOD = "dhcp"  ]]; then
	iptables -t nat -A POSTROUTING -o $WAN_BKP_IFACE -j MASQUERADE
fi

# http????
# vpn: 1194
iptables -A INPUT -i $WAN_IFACE -p udp --dport 1194 -j ACCEPT

if [[ $WAN_BKP_METHOD = "static" || $WAN_BKP_METHOD = "dhcp"  ]]; then
	iptables -A INPUT -i $WAN_BKP_IFACE -p udp --dport 1194 -j ACCEPT
fi

iptables -A INPUT -i $WIRELESS_IFACE -p udp --dport 1194 -j ACCEPT

# INPUT
iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT

# WAN forwarding
iptables -A FORWARD -i $LAN_IFACE -o $WAN_IFACE -j ACCEPT
iptables -A FORWARD -i $WAN_IFACE -o $LAN_IFACE -m state --state ESTABLISHED,RELATED -j ACCEPT
# bridge forwarding
iptables -A FORWARD -i $LAN_IFACE -o $LAN_IFACE -j ACCEPT

# WAN_BKP forwarding
if [[ $WAN_BKP_METHOD = "static" || $WAN_BKP_METHOD = "dhcp"  ]]; then
	iptables -A FORWARD -i $LAN_IFACE -o $WAN_BKP_IFACE -j ACCEPT
	iptables -A FORWARD -i $WAN_BKP_IFACE -o $LAN_IFACE -m state --state ESTABLISHED,RELATED -j ACCEPT
fi


# LAN_AUX forwarding
if [[ $LAN_AUX_METHOD = "static" ]]; then
	iptables -A FORWARD -i $LAN_AUX_IFACE -o $WAN_IFACE -j ACCEPT
	iptables -A FORWARD -i $WAN_IFACE -o $LAN_AUX_IFACE -m state --state ESTABLISHED,RELATED -j ACCEPT

# LAN to LAN_AUX forwarding
	iptables -A FORWARD -i $LAN_AUX_IFACE -o $LAN_IFACE -m state --state ESTABLISHED,RELATED -j ACCEPT
	iptables -A FORWARD -i $LAN_IFACE -o $LAN_AUX_IFACE -m state --state ESTABLISHED,RELATED -j ACCEPT
fi


# LAN3 OPENVPN maybe more?
if [[ $LAN3_METHOD = 'static' ]]; then
	iptables -A INPUT -i $LAN_AUX_IFACE -p udp -m udp --dport 53 -j ACCEPT
	
	if [[ $LAN3_TYPE -eq 0 ]]; then
		iptables -A INPUT -i $LAN3_IFACE -p udp --dport 1194 -j ACCEPT
	fi
	
	if [[ $LAN3_TYPE -eq 1 ]]; then
		iptables -A FORWARD -i $LAN3_IFACE -o $WAN_IFACE -j ACCEPT
		iptables -A FORWARD -i $WAN_IFACE -o $LAN3_IFACE -m state --state ESTABLISHED,RELATED -j ACCEPT
	fi

fi

# whitelist our IPs
for i in $WHITELIST; do
  iptables -A INPUT -p tcp -m tcp --dport 22 -s $i -j ACCEPT
done

iptables -N LIMIT_INDIVIDUAL
iptables -N LIMIT_OVERALL

iptables -A INPUT -p tcp --dport 22 --syn -m state --state NEW -j LIMIT_INDIVIDUAL
iptables -A INPUT -p tcp --dport 22 -m state --state ESTABLISHED -j ACCEPT

# for each IP limit simultaneous connections to INDILIM
iptables -A LIMIT_INDIVIDUAL -p tcp --dport 22 -m connlimit --connlimit-above $INDILIM -j DROP
iptables -A LIMIT_INDIVIDUAL -p tcp --dport 22 -m recent --set
iptables -A LIMIT_INDIVIDUAL -p tcp --dport 22 -m recent --update --seconds 60 --hitcount $INDILIM_MINUTE -j DROP
iptables -A LIMIT_INDIVIDUAL -j LIMIT_OVERALL

# for all IPs (except the whitelist) limit the number of ESTABLISHED connections
iptables -A LIMIT_OVERALL -m limit --limit $OVERLIM_MINUTE/minute -j ACCEPT 
iptables -A LIMIT_OVERALL -j DROP

# Example result:
# up to 10 connections in state NEW (ie, attempts) within a moving window of 1min for each IP (additional ones are dropped);
# up to 3 simultaneous connections obtained slowly for each IP (connections before time are blocked by the conditions above)
# up to a total (all IPs, except whitelist) of 15 connections in state NEW within a moving window of 1min (additional ones are dropped); mitigates distributed bf attacks)

# SAVING IPTABLES
iptables-save > /etc/iptables/rules.v4

# PORTFORWARDING DE SERVERS
cp $PORT_FORWARDING_FILE $LOCAL_BIN_DIR/run_port_forwarding
replace_expr $LOCAL_BIN_DIR/run_port_forwarding "<LOCAL_BIN_DIR>" $LOCAL_BIN_DIR
replace_expr $LOCAL_BIN_DIR/run_port_forwarding "<LOCAL_ETC_DIR>" $LOCAL_ETC_DIR

cp $PORT_FORWARD_LIST_FILE $LOCAL_ETC_DIR/port_forwards

# LAN FORWARDING
cp $LAN_FORWARDING_FILE $LOCAL_BIN_DIR/run_lan_forwarding
replace_expr $LOCAL_BIN_DIR/run_lan_forwarding "<LOCAL_BIN_DIR>" $LOCAL_BIN_DIR
replace_expr $LOCAL_BIN_DIR/run_lan_forwarding "<LOCAL_ETC_DIR>" $LOCAL_ETC_DIR

cp $LAN_FORWARD_LIST_FILE $LOCAL_ETC_DIR/lan_forwards

cp ./src/etc/post_apply $LOCAL_BIN_DIR/

# setup update file
cp $APPLY_FWD_CHANGES $LOCAL_BIN_DIR/apply_forward_changes
replace_expr $LOCAL_BIN_DIR/apply_forward_changes "<LOCAL_BIN_DIR>" $LOCAL_BIN_DIR

chmod 755 $LOCAL_BIN_DIR/run_lan_forwarding
chmod 755 $LOCAL_BIN_DIR/run_port_forwarding
chmod 755 $LOCAL_BIN_DIR/apply_forward_changes
chmod 755 $LOCAL_BIN_DIR/post_apply

echo "RESTARTING NETWORKING SERVICE..."
systemctl restart networking.service &> /dev/null
systemctl restart netfilter-persistent
#----------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------------------

### OPEN VPN SETUP ###
#----------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------------------
echo "INSTALLING OPEN VPN..."
apt-get install -y openvpn
if [[ ! -d "/etc/openvpn/keys" ]]; then
	mkdir /etc/openvpn/keys
fi
cp src/openvpn/keys/dh1024.pem /etc/openvpn/keys
cp src/openvpn/keys/ca.crt /etc/openvpn/keys
cp src/openvpn/keys/server.crt /etc/openvpn/keys
cp src/openvpn/keys/server.key /etc/openvpn/keys

cp $OPEN_VPN_SERVER_CONF ${OPEN_VPN_SERVER_CONF}_temp

replace_expr ${OPEN_VPN_SERVER_CONF}_temp "<LAN_STATIC_IP>" $LAN_STATIC_IP
replace_expr ${OPEN_VPN_SERVER_CONF}_temp "<LAN_NETMASK>" $LAN_NETMASK
replace_expr ${OPEN_VPN_SERVER_CONF}_temp "<VPN_POOL_START>" $VPN_POOL_START
replace_expr ${OPEN_VPN_SERVER_CONF}_temp "<VPN_POOL_END>" $VPN_POOL_END
replace_expr ${OPEN_VPN_SERVER_CONF}_temp "<LAN_IFACE>" $LAN_IFACE
replace_expr ${OPEN_VPN_SERVER_CONF}_temp "<LAN_BRIDGE_PORT>" $LAN_BRIDGE_PORT

if [[ $WINBIND -eq 1 ]]; then
	replace_expr ${OPEN_VPN_SERVER_CONF}_temp "<PUSH_DNS_IP>" $DOMAIN_SERVER_IP
else
	replace_expr ${OPEN_VPN_SERVER_CONF}_temp "<PUSH_DNS_IP>" $LAN_STATIC_IP
fi

cp ${OPEN_VPN_SERVER_CONF}_temp /etc/openvpn/server.conf
rm ${OPEN_VPN_SERVER_CONF}_temp

cp $BRIDGE_UP_SCRIPT /etc/openvpn/

chmod 755 /etc/openvpn/up.sh

### files related with openvpn logging ###

cp $RSYSLOG_OPENVPN /etc/rsyslog.d/
chmod 644 /etc/rsyslog.d/20-openvpn.conf

cp $LOG_ROTATE_OVPN /etc/logrotate.d/
chmod 644 /etc/logrotate.d/ovpn

echo "RESTARTING RSYSLOG..."
systemctl restart rsyslog

##########################################

echo "DISABLING and RESTARTING OPENVPN..."
systemctl disable openvpn
systemctl restart openvpn@server.service
#----------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------------------

### HOSTAPD SETUP ###
#----------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------------------
echo "INSTALLING HOSTAPD..."
apt-get install -y hostapd

echo 'DAEMON_CONF="/etc/hostapd/hostapd.conf"' > /etc/default/hostapd

cp $HOSTAPD_CONF_SKEL_FILE /etc/hostapd/hostapd.conf
replace_expr /etc/hostapd/hostapd.conf "<WIRELESS_IFACE>" $WIRELESS_IFACE
replace_expr /etc/hostapd/hostapd.conf "<SSID>" $SSID
chmod 644 /etc/hostapd/hostapd.conf


echo "RESTARTING HOSTAPD..."
systemctl restart hostapd
#----------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------------------

### WINBIND AND NTP SETUP ###
#----------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------------------
echo "INSTALLING NTP..."
apt-get install -y ntp

replace_expr /etc/ntp.conf "pool 0.ubuntu.pool.ntp.org" "server ntp.angulosolido.pt"
replace_expr /etc/ntp.conf "pool 1.ubuntu.pool.ntp.org" ""
replace_expr /etc/ntp.conf "pool 2.ubuntu.pool.ntp.org" ""
replace_expr /etc/ntp.conf "pool 3.ubuntu.pool.ntp.org" ""

systemctl restart ntp
echo "RESTARTING NTP..."


##### REPLACE TMP DNS #####
replace_for_multiple /etc/network/interfaces $TMP_DNS_SERVER $DNS_SERVERS_IP

# these commands should force /etc/resolv.conf update
ifdown $LAN_IFACE
sleep $WAIT_FOR_IFDOWN
ifup $LAN_IFACE
###########################


# check for winbind "active bit"
if [[ $WINBIND -eq 1 ]]; then
	echo "INSTALLING WINBIND..."
	apt-get install -y winbind
	apt-get install -y libnss-winbind
	apt-get install -y libpam-winbind

	cp $SMB_CONF_SKEL /etc/samba/smb.conf
	replace_expr /etc/samba/smb.conf "<DOMAIN_SERVER>" $DOMAIN_SERVER
	replace_expr /etc/samba/smb.conf "<TLD>" $TLD
	replace_expr /etc/samba/smb.conf "<NETBIOS_NAME>" $NETBIOS_NAME
	replace_expr /etc/samba/smb.conf "<HOST_NAME>" $HOST_NAME
	replace_expr /etc/samba/smb.conf "<SMB_CACHE_LIFETIME>" $SMB_CACHE_LIFETIME
	mkdir /var/log/samba

	echo "MOVING openvpn pam file into etc/pam.d/"
	cp $PAM_OPENVPN /etc/pam.d/

	replace_expr /etc/openvpn/server.conf "passwd" "openvpn"

	echo "SETTING CUSTOM nsswitch.conf file AND STORING nsswitch.conf.orig"
	cp $NSSWITCH /etc/
	cp ${NSSWITCH}.orig /etc/

	echo "STOPING WINBIND..."
	systemctl stop winbind

	cp $SMB_DIR/wb-start.sh $LOCAL_BIN_DIR
	cp $SMB_DIR/wb-stop.sh $LOCAL_BIN_DIR

	chmod 755 $LOCAL_BIN_DIR/wb-stop.sh
	chmod 755 $LOCAL_BIN_DIR/wb-start.sh

	cp $SMB_DIR/pam_winbind.conf_skel /etc/security/pam_winbind.conf
	replace_expr /etc/security/pam_winbind.conf "<GROUPS>" $GROUPS_ALLOWED

	cp $SMB_DIR/10_domain-admins /etc/sudoers.d/
	chmod 0440 /etc/sudoers.d/10_domain-admins

	# join to domain
	echo "JOINING DOMAIN..."
	$LOCAL_BIN_DIR/wb-start.sh

	echo "RESTARTING WINBIND..."
	systemctl start winbind

else
	apt-get purge -y winbind
	cp /etc/nsswitch.conf.orig /etc/nsswitch.conf >& /dev/null
	echo "NO WINBIND CONFIGURATION!!!"
fi

#----------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------------------


### CAKE SETUP ###
#----------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------------------

# swap files function
swap_4_new() {

	if [ "$#" -lt 3 ]; then
		echo "USAGE: swap_4_new <action> <src_directory> <dest_directory> {gz}"
		return
	fi

	ACTION=$1
	SRC_DIR=$2
	DEST_DIR=$3
	AFIX=""
	if [[ $4 = "gz" ]]; then
		AFIX=".gz"
	fi


	echo "copying $2 to $3"
	if [ $ACTION = "new" ]; then
		cp -f $SRC_DIR $DEST_DIR >& /dev/null

	elif [ $ACTION = "bkp" ]; then
		mv $DEST_DIR$AFIX $DEST_DIR$AFIX.orig >& /dev/null
		cp -f $SRC_DIR $DEST_DIR >& /dev/null
		echo "with backup: $DEST_DIR$AFIX.orig"
	fi

	# giving permissions to scripts
	echo $SRC_DIR | grep -q "${QOS_DIR_SUPP}/sbin/"

	if [[ $? -eq 0 ]]; then
	   chmod 755 $DEST_DIR
	fi

	echo "---------------"
}

##### MAKING THE KERNEL DOWNGRADE #####
KERNEL_VERSION="4.4.0-98"
./bin/fix_kernel.sh $KERNEL_VERSION

### CAKE ITSELF ###
cp -f $QOS_DIR/qos.sh $LOCAL_BIN_DIR
chmod 755 $LOCAL_BIN_DIR/qos.sh

replace_expr $LOCAL_BIN_DIR/qos.sh "<LOCAL_ETC_DIR>" $LOCAL_ETC_DIR


cp -f $QOS_DIR/sch_cake.ko /lib/modules/$KERNEL_VERSION-generic/kernel/net/sched/
chmod 644 /lib/modules/$KERNEL_VERSION-generic/kernel/net/sched/sch_cake.ko
depmod "$KERNEL_VERSION-generic"

echo "sch_cake" >> /etc/modules
echo "act_mirred" >> /etc/modules

### SUPPORT FOR CAKE ###

mkdir /var/lib/arpd
envsubst < $QOS_DIR/lista_actions >> temp_file

{ cat temp_file; echo; } | while read line; do
		swap_4_new $line
done

rm temp_file

### SSH GROUPS SETUP ###
#----------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------------------

cp $SSHD_CONFIG_FILE /etc/ssh/sshd_config
cp $SSHD_CONFIG_FILE /etc/ssh/sshd_config.orig


echo "AllowGroups $SSH_GROUPS_LOCAL $SSH_GROUPS_DC" >> /etc/ssh/sshd_config

#----------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------------------


apt-get install -y lm-sensors
apt-get install -y vim



cat $LOG_FILE

echo
echo
echo "###############################################################"
echo "#                                                             #"
echo "# ITS ADVISED A SYSTEM REBOOT TO ENSURE PROPPER INSTALLATION! #"
echo "#                                                             #"
echo "###############################################################"
echo
echo
while true; do
    read -p "Do you wish to reboot the system? (yes no) " yn
    case $yn in
        "Yes" | "yes" | "YES" ) echo "######### REBOOT ##########"; reboot; break;;
        "No" | "no" | "NO") exit;;
        * ) echo "Please answer yes or no.";;
    esac
done
