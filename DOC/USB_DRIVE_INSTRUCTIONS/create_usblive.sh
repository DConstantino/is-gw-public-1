# the file mbr.bin was copied from a packages of syslinux downloaded from this link: https://www.kernel.org/pub/linux/utils/boot/syslinux
#the file is located in: syslinux-4.04/mbr/mbr.bin

# this was the version i used
# wget https://www.kernel.org/pub/linux/utils/boot/syslinux/syslinux-4.04.tar.bz2

# get the tinycore package from pc engines
# wget http://pcengines.ch/file/apu2-tinycore6.4.tar.bz2

# get the Ubuntu Xenial 16.04 modified files from pc engines
# wget http://pcengines.ch/tmp/ubuntu16.04_serial.tar.bz2

# move both the archives and the mbr.bin file into the same directory

#set these variables first
FILESFIR=$(pwd) #use different directory to run the script
DEVICE="/dev/sdb"


#format and partition
umount ${DEVICE}1
dd if=/dev/zero of=${DEVICE} count=1 conv=notrunc
echo -e "o\nn\np\n1\n\n\nw" | fdisk ${DEVICE}
mkfs.vfat -n XENIAL_APU -I ${DEVICE}1

#make the device bootable
syslinux -i ${DEVICE}1
dd conv=notrunc bs=440 count=1 if=mbr.bin of=${DEVICE}
parted ${DEVICE} set 1 boot on


#add tiny core
mount ${DEVICE}1 /mnt
cd /mnt
tar -C /mnt xvjf $FILESFIR/apu_tinycore.tar.bz2

#unpack modified files
tar -C /mnt xxjf $FILESFIR/ubuntu16.04_serial.tar.bz2
cd
umount /mnt