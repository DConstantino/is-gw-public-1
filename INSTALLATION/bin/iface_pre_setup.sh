#!/bin/bash

. ./interface_aux.sh  --source-only

set -a
. ../gw.conf
set +a



re='^[0-9]+$'

setup_start() {
echo "# $(date)"

echo_static_iface_dns $LAN_BRIDGE_PORT $LAN_STATIC_IP $LAN_NETWORK $LAN_NETMASK $LAN_BROADCAST 8.8.8.8

if [ "$WAN_METHOD" = "dhcp" ]
		then
			if [ "$WAN_VLAN_ID" = "" ]
				then
					echo_dhcp_iface $WAN_IFACE
			elif ! [[ "$WAN_VLAN_ID" =~ $re ]]
				then
					echo "ERROR: bad config file!!! Location: WAN vlan id\n" >> $LOG_FILE
					exit 1
			else
				echo_dhcp_iface_vlan $WAN_IFACE $WAN_VLAN_ID
			fi

	elif [ "$WAN_METHOD" = "static" ]
		then
			if [ "$WAN_VLAN_ID" = "" ]
				then
					echo_wan_static_iface $WAN_IFACE $WAN_STATIC_IP $WAN_NETWORK $WAN_NETMASK $WAN_BROADCAST $WAN_GATEWAY
			elif ! [[ "$WAN_VLAN_ID" =~ $re ]]
				then
					echo "ERROR: bad config file!!! Location: WAN vlan id\n" >> $LOG_FILE
					exit 1
			else
				echo_wan_static_iface_vlan $WAN_IFACE $WAN_STATIC_IP $WAN_NETMASK $WAN_VLAN_ID $WAN_BROADCAST $WAN_GATEWAY
			fi

	elif [ "$WAN_METHOD" = "none" ]
		then
			echo "No WAN interface config!\n" >> $LOG_FILE
	else
		echo "ERROR: bad config file!!! Location: WAN interface.\n" >> $LOG_FILE
		exit 1
	fi

}


run_main() {

	setup_start > /etc/network/interfaces

	# due to Ubuntu 16.04 specifity need to FLUSH old IP address
	# in order to switch between different static IPs
	# systemctl restart does NOT get this job done

	ifdown $CURRENT_IFACE
	ip addr flush $CURRENT_IFACE
	ifup $CURRENT_IFACE

	ifdown $WAN_IFACE
	ip addr flush $WAN_IFACE
	ifup $WAN_IFACE

	# affetcts WAN infterface
	systemctl restart networking

}

CURRENT_IFACE=$LAN_BRIDGE_PORT

# output enp1s0 IP
CURRENT_LAN_IP="$(ip -4 -o  addr list $CURRENT_IFACE | awk '{print $4}' | awk -F/ '{print $1}')"

echo
echo "###########################################################"
echo "The current IP for enp1s0 interface is: $CURRENT_LAN_IP"
echo "This script will set $CURRENT_IFACE iface with this IP: $LAN_STATIC_IP"
echo "##########################################################"
echo

echo "If the new IP is different you will need to exit from this session and run:

	ssh ubuntu@$LAN_STATIC_IP

"
echo "Then you should check for DNS resolution and Internet access:

	ping angulosolido.pt
	ping google.pt
"

echo
echo "#################################################################"
echo "#                                                               #"
echo "# THIS SCRIPT WILL MAKE CHANGES TO /etc/network/interfaces FILE #"
echo "#                                                               #"
echo "#################################################################"
echo
echo
while true; do
    read -p "Are you sure you want to run this? (yes no) " yn
    case $yn in
        "Yes" | "yes" | "YES" ) run_main; break;;
        "No" | "no" | "NO") exit;;
        * ) echo "Please answer yes or no.";;
    esac
done

