#!/bin/bash

read -p "Username: " JOINUSER
read -sp "Password: " JOINPASS;echo

rm /var/cache/samba/netsamlogon_cache.tdb >& /dev/null
rm /var/lib/samba/winbindd_cache.tdb >& /dev/null
rm /var/lib/samba/group_mapping.tdb >& /dev/null

net join -U${JOINUSER}%${JOINPASS}

service smbd start
service winbind start
# acts on gencache.tdb
net cache flush