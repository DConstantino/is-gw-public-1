 #!/bin/bash

RC_ERR=1
#UPDATE_CMD=/usr/local/AS/bin/apt-update-pkg.sh
#SURFACE="openssh-server openssh-client openssh-sftp-server nginx openssl libudev1 ca-certificates"
UPDATE_CMD="./apt-update-pkg.sh"
SURFACE="bind9 bind9-host bind9utils bridge-utils ca-certificates dnsutils hostapd iptables isc-dhcp-client isc-dhcp-common isc-dhcp-server libnss-winbind:amd64 openssh-client openssh-server openssh-sftp-server openssl openvpn samba samba-common samba-common-bin samba-dsdb-modules samba-libs:amd64 samba-vfs-modules ssh-import-id ufw vlan winbind wireless-regdb libgnutls-openssl27:amd64 ntp netcat-openbsd"

apt-get update 2>&1 > /dev/null
RC=$?

if [ ! $RC -eq 0 ]; then
  echo "error running apt-get update"
  exit $RC
fi

for p in $SURFACE; do
  $UPDATE_CMD $p
done

exit 0
