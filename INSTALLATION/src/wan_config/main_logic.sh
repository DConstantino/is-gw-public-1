#!/bin/bash


LOCAL_ETC_DIR="<LOCAL_ETC_DIR>"
LOCAL_BIN_DIR="<LOCAL_BIN_DIR>"
LOCAL_LIB_DIR="<LOCAL_LIB_DIR>"
ETC_NETWORKING_DIR="/etc/network"

. ${LOCAL_ETC_DIR}/dynamic_vars --source-only
. ${LOCAL_LIB_DIR}/interface_aux.sh --source-only
. ${LOCAL_LIB_DIR}/wan_config/source_file --source-only


WAN_BKP_METHOD=""
if [ x$1 = "x-b" ]; then
    declare "OLD_IFACE"="$WAN_BKP_IFACE"
    declare "OLD_METHOD"="$WAN_BKP_METHOD"
    declare "OLD_UPSTREAM"="$WAN_BKP_UPSTREAM"
    declare "OLD_NETWORK"="$WAN_BKP_NETWORK"
    INTERFACE_STRING="WAN_BKP"
else
    declare "OLD_IFACE"="$WAN_IFACE"
    declare "OLD_METHOD"="$WAN_METHOD"
    declare "OLD_UPSTREAM"="$WAN_UPSTREAM"
    declare "OLD_NETWORK"="$WAN_NETWORK"
    INTERFACE_STRING="WAN"
fi


CUR_DATE=`date +%Y-%b-%d-%Hh%Mm`

START_LINE=`cat ${ETC_NETWORKING_DIR}/interfaces | grep -n "auto ${OLD_IFACE}" | awk -F ':' '{print $1}'`

head -n $(( ${START_LINE} - 1 )) ${ETC_NETWORKING_DIR}/interfaces > /tmp/temp_head

# find out if the current
# wan interface has a vlan
has_vlan $OLD_IFACE
VLAN=$?

iface_conf_size $START_LINE $OLD_METHOD $VLAN
END_LINE=$?
tail --lines=+$(( ${END_LINE} + 1 )) ${ETC_NETWORKING_DIR}/interfaces > /tmp/temp_tail

# create new interface configuration
####################################
NEW_VLAN_NM=`echo ${NEW_WAN_IFACE} | awk -F '.' '{print $2}'`

if [ ${NEW_WAN_METHOD} = 'static' ];then

    if [ z${NEW_VLAN_NM} != "z" ];then
        echo_wan_static_iface_vlan $NEW_WAN_IFACE $NEW_WAN_STATIC_IP $NEW_WAN_NETMASK $NEW_WAN_NETWORK $NEW_WAN_BROADCAST $NEW_WAN_GATEWAY > /tmp/temp_new_iface
    else
        echo_wan_static_iface $NEW_WAN_IFACE $NEW_WAN_STATIC_IP $NEW_WAN_NETWORK $NEW_WAN_NETMASK $NEW_WAN_BROADCAST $NEW_WAN_GATEWAY > /tmp/temp_new_iface
    fi
    
elif [ ${NEW_WAN_METHOD} = 'dhcp' ];then

    if [ z${NEW_VLAN_NM} != "z" ];then
        echo_dhcp_iface_vlan $NEW_WAN_IFACE > /tmp/temp_new_iface
    else
        echo_dhcp_iface $NEW_WAN_IFACE > /tmp/temp_new_iface
    fi
fi

# make sure that there is an empty line
# after the new interface configuration
echo "" >> /tmp/temp_new_iface
####################################

# clean up
cat /tmp/temp_head > /tmp/new_interfaces_file
cat /tmp/temp_new_iface >> /tmp/new_interfaces_file;
cat /tmp/temp_tail >> /tmp/new_interfaces_file
rm /tmp/temp_head /tmp/temp_new_iface /tmp/temp_tail
#############

ifdown $OLD_IFACE &> /dev/null
ip addr flush $OLD_IFACE &> /dev/null

# place the new interfaces file
cp ${ETC_NETWORKING_DIR}/interfaces ${ETC_NETWORKING_DIR}/interfaces-${CUR_DATE}.bkp
cat -s /tmp/new_interfaces_file > ${ETC_NETWORKING_DIR}/interfaces

ifup $NEW_WAN_IFACE &> /dev/null

# update the firewall rules with the new interface
update_firewall "$OLD_IFACE" "$NEW_WAN_IFACE"


######################
cp $LOCAL_BIN_DIR/wan_monitor_aux.sh $LOCAL_BIN_DIR/wan_monitor_aux-${CUR_DATE}.sh.bkp
cp $LOCAL_ETC_DIR/dynamic_vars $LOCAL_ETC_DIR/dynamic_vars-${CUR_DATE}.bkp


replace_expr $LOCAL_BIN_DIR/wan_monitor_aux.sh "${INTERFACE_STRING}_IFACE=${OLD_IFACE}" "${INTERFACE_STRING}_IFACE=$NEW_WAN_IFACE"

replace_expr $LOCAL_ETC_DIR/dynamic_vars "${INTERFACE_STRING}_IFACE=${OLD_IFACE}" "${INTERFACE_STRING}_IFACE=$NEW_WAN_IFACE"
replace_expr $LOCAL_ETC_DIR/dynamic_vars "${INTERFACE_STRING}_METHOD=${OLD_METHOD}" "${INTERFACE_STRING}_METHOD=$NEW_WAN_METHOD"
replace_expr $LOCAL_ETC_DIR/dynamic_vars "${INTERFACE_STRING}_UPSTREAM=${OLD_UPSTREAM}" "${INTERFACE_STRING}_UPSTREAM=$NEW_WAN_UPSTREAM"
replace_expr $LOCAL_ETC_DIR/dynamic_vars "${INTERFACE_STRING}_NETWORK=${OLD_NETWORK}" "${INTERFACE_STRING}_NETWORK=${NEW_WAN_NETWORK}"

##############################

if [ $NEW_WAN_METHOD = 'static' ];then
    echo $NEW_WAN_GATEWAY > ${LOCAL_ETC_DIR}/${NEW_WAN_IFACE}-gw
fi

mv $LOCAL_ETC_DIR/wan_iface_config.log $LOCAL_ETC_DIR/wan_iface_config.log.old &> /dev/null

LOG_STR="Configuration executed - `date`"

echo $LOG_STR >> $LOCAL_ETC_DIR/wan_iface_config.log
if [ ${NEW_WAN_METHOD} = 'static' ];then
    echo "  Interface           ->  $NEW_WAN_IFACE" >> $LOCAL_ETC_DIR/wan_iface_config.log
    echo "  Upstream bandwidth  ->  $NEW_WAN_UPSTREAM Mbit/s" >> $LOCAL_ETC_DIR/wan_iface_config.log
    echo "  Interface method    ->  $NEW_WAN_METHOD" >> $LOCAL_ETC_DIR/wan_iface_config.log
    echo "  Interface IP        ->  $NEW_WAN_STATIC_IP" >> $LOCAL_ETC_DIR/wan_iface_config.log
    echo "  Network             ->  $NEW_WAN_NETWORK" >> $LOCAL_ETC_DIR/wan_iface_config.log
    echo "  Netmask             ->  $NEW_WAN_NETMASK" >> $LOCAL_ETC_DIR/wan_iface_config.log
    echo "  Broadcast           ->  $NEW_WAN_BROADCAST" >> $LOCAL_ETC_DIR/wan_iface_config.log
    echo "  Gateway             ->  $NEW_WAN_GATEWAY" >> $LOCAL_ETC_DIR/wan_iface_config.log

elif [ $NEW_WAN_METHOD = 'dhcp' ];then
    echo "  Interface           ->  $NEW_WAN_IFACE" >> $LOCAL_ETC_DIR/wan_iface_config.log
    echo "  Upstream bandwidth  ->  $NEW_WAN_UPSTREAM Mbit/s" >> $LOCAL_ETC_DIR/wan_iface_config.log
    echo "  Interface method    ->  $NEW_WAN_METHOD" >> $LOCAL_ETC_DIR/wan_iface_config.log
fi

echo "" >> $LOCAL_ETC_DIR/wan_iface_config.log
cat $LOCAL_ETC_DIR/wan_iface_config.log.old >> $LOCAL_ETC_DIR/wan_iface_config.log &> /dev/null
