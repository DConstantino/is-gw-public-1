# returns:
# 0 if double wan
# 1 if single wan
# 2 if bad config file
check_double_wan(){
	if [ $WAN_METHOD != "none" ]; then
		if [ $WAN_BKP_METHOD != "none" ]; then
			return 0
		fi
		return 1
	fi
	return 2
}



# WAN ROUTE TABLE NUMBER ALWAYS 1
set_wan_routes() {

	CURRENT_WAN_IP="$(ip -4 -o  addr list $WAN_IFACE | awk '{print $4}' | awk -F/ '{print $1}')"
	

	if [ $WAN_METHOD = "dhcp" ]; then
		WAN_GW="$(cat /tmp/${WAN_IFACE}-gw)"
		CURRENT_WAN_NETWORK="$(get_network $WAN_IFACE cidr)"
	fi

	if [ $WAN_METHOD = "static" ]; then
		WAN_GW="$(cat /usr/local/AS/etc/${WAN_IFACE}-gw)"
		CURRENT_WAN_NETWORK=$WAN_NETWORK
	fi

	ip route flush table wan_table

	ip rule del from $CURRENT_WAN_IP table wan_table

	ip rule add from $CURRENT_WAN_IP table wan_table
	ip route add ${LAN_NETWORK}/24 dev $LAN_IFACE src $CURRENT_WAN_IP table wan_table
	ip route add $WAN_GW dev $WAN_IFACE src $CURRENT_WAN_IP table wan_table
	ip route add default via $WAN_GW table wan_table

	ip route add $CURRENT_WAN_NETWORK dev $WAN_IFACE table wan_table

	route | grep default
	res=$?
	if [[ res -eq 0 ]]; then
		ip route change default via $WAN_GW
	else
		ip route add default via $WAN_GW
	fi
}




# WAN BKP ROUTE TABLE NUMBER ALWAYS 2
set_wan_bkp_routes() {

	CURRENT_WAN_BKP_IP="$(ip -4 -o  addr list $WAN_BKP_IFACE | awk '{print $4}' | awk -F/ '{print $1}')"
	
	if [ $WAN_BKP_METHOD = "dhcp" ]; then
		WAN_BKP_GW="$(cat /tmp/${WAN_BKP_IFACE}-gw)"
		CURRENT_WAN_BKP_NETWORK="$(get_network $WAN_BKP_IFACE cidr)"
	fi

	if [ $WAN_BKP_METHOD = "static" ]; then
		WAN_BKP_GW="$(cat /usr/local/AS/etc/${WAN_BKP_IFACE}-gw)"
		CURRENT_WAN_BKP_NETWORK=$WAN_BKP_NETWORK
	fi

	ip route flush table wan2_table

	ip rule del from $CURRENT_WAN_BKP_IP table wan2_table

	ip rule add from $CURRENT_WAN_BKP_IP table wan2_table
	ip route add ${LAN_NETWORK}/24 dev $LAN_IFACE src $CURRENT_WAN_BKP_IP table wan2_table
	ip route add $WAN_BKP_GW dev $WAN_BKP_IFACE src $CURRENT_WAN_BKP_IP table wan2_table
	ip route add default via $WAN_BKP_GW table wan2_table

	ip route add $CURRENT_WAN_BKP_NETWORK dev $WAN_BKP_IFACE table wan2_table

}


set_servers_routes() {

	for i in $SERVERS; do
		ip rule del from $i table wan2_table
		ip rule add from $i table wan2_table
	done
}


# port forwarding for special servers
# cmd entry_iface protocol entry_port dest_ip dest_port
# 1		2			3			4		5		6
forward_service() {

	if [ "$#" -ne 6 ]; then
		exit
	fi

	DEST=$5
	DPORT=$4
	DPORTINT=$6
	DPROTO=$3
	IF=$2
	CMD=-A

	if [ $1 = "del" ]; then
		CMD=-D
	fi

	iptables -t nat $CMD PREROUTING -i $IF -p $DPROTO -m $DPROTO --dport $DPORT -j DNAT --to-destination $DEST:$DPORTINT
	iptables -t filter $CMD FORWARD -d $DEST -i $IF -p $DPROTO -m $DPROTO --dport $DPORTINT -j ACCEPT

}

# $1 = destination ip
# $2 = input iface
# $3 = protocol
# $4 = destination port
add_lan_forwarding() {
	if [ "$#" -ne 4 ]; then
		exit
	fi

	iptables -A FORWARD -d $1 -i $2 -p $3 -m $3 --dport $4 -j ACCEPT
}

# self explainable name
# $1 = interface $2 = cidr ( '/24' notation )
get_network() {


	MY_MASK="$(ifconfig "$1" | sed -rn '2s/ .*:(.*)$/\1/p')"
	MY_IP="$(ip -4 -o  addr list $1 | awk '{print $4}' | awk -F/ '{print $1}')"

	echo $MY_IP > tmp_ip_file
	echo $MY_MASK > tmp_mask_file

	OLD_IFS=$IFS
	IFS=.
	read -r i1 i2 i3 i4 <"./tmp_ip_file"

	IFS=.
	read -r m1 m2 m3 m4 <"./tmp_mask_file"

	IFS=$OLD_IFS

	rm tmp_ip_file
	rm tmp_mask_file

	if [ $2 = "cidr" ]; then
		printf "%d.%d.%d.%d/%s\n" "$((i1 & m1))" "$((i2 & m2))" "$((i3 & m3))" "$((i4 & m4))" "$(netmask2cidr $MY_MASK)"
		return
	fi

	printf "%d.%d.%d.%d\n" "$((i1 & m1))" "$((i2 & m2))" "$((i3 & m3))" "$((i4 & m4))"
}

# netmask notation converter
# ex.: converts '255.255.255.0' to '/24'
# credit where credit's due: https://gist.github.com/tiveone/2506075
netmask2cidr()
{ 
  case $1 in
      0x*)
      local hex=${1#0x*} quad=
      while [ -n "${hex}" ]; do
        local lastbut2=${hex#??*}
        quad=${quad}${quad:+.}0x${hex%${lastbut2}*}
        hex=${lastbut2}
      done
      set -- ${quad}
      ;;
  esac

  local i= len=
  local IFS=.
  for i in $1; do
    while [ ${i} != "0" ]; do
      len=$((${len} + ${i} % 2))
      i=$((${i} >> 1))
    done
  done

  echo "${len}"
}